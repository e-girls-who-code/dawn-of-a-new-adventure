# Dawn of a new Adventure


## Launch the game

You will find the game in the Dawn of a new Adventure zip directory.

Load the DragonGame.exe file and enjoy !

The game was made with Unity 2D. 

## How to play ? 

Use your arrow keys to move or WASD

Use space to jump

## Playtest questionnary

https://forms.gle/E1VNDc2f3HNmMkS49
